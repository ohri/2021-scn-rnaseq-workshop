## SCN RNASeq Workshop 2021: IGV Tutorial

This lab will introduce you to the Integrative Genomics Viewer, a useful tool for visualizing high throughput sequencing data in a genomic context.

You should have already downloaded the following files; if not download into the same directory.
* [in_day1_rep1_chr12.bam](in_day1_rep1_chr12.bam)
* [in_day1_rep1_chr12.bam.bai](in_day1_rep1_chr12.bai)

The first file (.bam) contains the genomic alignments of the reads. 
The mapped reads in the bam file are sorted in genomic order, by chromosome and location, and the associated index (.bai) references positions in the .bam file allowing quick access.
Note that the provided bam file contains mapped reads for only chromosome 12 in order to reduce the size for ease of download.

## Setting Genomic Assembly

* Start by open IGV, which you should already have installed on your laptop.

By default IGV opens set to use the hg19 human genome reference. 
It is important to use the correct reference genome! 
Reference genome assemblies are periodically updated. 
These updates modify the genomic assembly, changing coordinates features.
Care must be taken to ensure that the coordinates you're _viewing_ the data in are the same as which it was generated. 
The data used in this exercise is aligned to the human hg38 assembly.

* On the top left of the IGV browser, where it indicates `hg19`, click and scroll to `more`
* In the dialog that appears, find the `Human hg38` option, select it and click `OK`. This may take a couple of minutes to download.

## Loading Reference tracks

IGV displays a RefSeq gene track by default, but you can additional reference tracks easily.

* Select File | Load From Server
  * Select "Ensembl Genes" under annotations. This may take a little while to load
* Once it has loaded, select `chr12` from the pull down next to the genome pull down at the top of the IGV display.
* Type `TBK1` in the window to the right of the chromosome pull down. You have nagivated to a genomic position by gene name.
* Use the [+]/[-] zoom buttons on the top right to zoom in on the data and explore the gene mapping

*How does the "Ensembl Genes" track differ from the Gene track? Why does it differ?*

## Loading and viewing aligned data

* To load the above bam file (not the .bai which is the index, it will automatically find that) use the menu item: 
    * File | Load From File
* select the `in_day1_rep1_chr12.bam` file
* Paste `chr12:64,500,768-64,502,412`. This allows you to navigate to a genomic position by genomic coordinates of the form `chromosome:start-end`
* The bars you see in the right side of the display next to the `Files_in_day1_rep1_chr12.bam` label on the left hand side are reads mapped to those genomic coodinates.
* Above that the "Coverage" plot (wiggly line) indicates the depth of mapped reads covering each position. You can see how they are higher over the exons, where we expect RNASeq data to map.
* right click on the `Files_in_day1_rep1_chr12.bam` panel on the left and select `View as pairs`. This displays the mapped reads as paired and connects them visually.

*Why are there reads mapped to the introns?*

## Inspecting reads
* Move your mouse over an aligned read pair and you'll see details of the read and read mapping

*Why are some pairs green? What does that appear to represent?*

* right click on the `Files_in_day1_rep1_chr12.bam` panel on the left and select `Show mismatched bases`. 
* Scroll around and inspect reads for mismatched bases, which show up as colored bars. 

*In some cases we see stacks of mismatched bases at one position in the genome, in approximately half of reads covering that position. What could cause this?*









