# 2021 Stem Cell Network RNASeq Workshop

## Workshop Dates (October)
### R Language Introduction: October 18 (11-2 ET)
### Session 3: October 21-22, Optional Tutorial Day October 29 (11-5 ET)

## Workshop Dates (June)
### R Language Introduction: June 4 (11-2 ET)
### Session 1: June 9-10, Optional Tutorial Day June 11 (11-5 ET)
### Session 2: June 16-17, Optional Tutorial Day June 18 (11-5 ET)


## Workshop Venue

A Zoom meeting will be sent to all meeting participants before each session.

## Organizers		
Dr. Bill Stanford (uOttawa/OHRI)  
Dr. Ted Perkins (uOttawa/OHRI)  

## Workshop Files

* [October Workshop Agenda](Files/October_2021_RNASeq_Workshop_Agenda.pdf?inline=false)
* [June Workshop Agenda](Files/2021_RNA-Seq_Analysis_Workshop_Agenda.pdf?inline=false)


### R Language Introduction
 
* [R walkthrough commands](Files/Rwalkthrough.Rmd)
* Introduction to R Packages ([PowerPoint](Files/Intro_to_R_Packages.pptx?inline=false))
* [Library installation required for Day 1 & 2 exercises](Files/R_package_installation.txt)

### RNASeq Workshop

### Day 1:

Workshop and Goals (Dr. Bill Stanford) [PowerPoint](Files/RNA-seq_workshop_Stanford_2021.pptx)

RNA-seq Basics (Gareth Palidwor) [PowerPoint](Files/2021_Workshop_Lecture_1_RNASeq_Basics.pptx?inline=false)
* Example FASTQC file
    * [ERR975344.1_fastqc](http://www.ogic.ca/projects/workshop_2019/FastQC/ERR975344.1_fastqc.html?inline=false)
* Example multiqc file
    * [MultiQC report](http://www.ogic.ca/projects/workshop_2019/multiqc/multiqc_report.html?infile=false)
* Example BAM files for [IGV](Files/IGV.md) visualization
    * [in_day1_rep1_chr12.bam](https://gitlab.com/ohri/2019-SCN-rnaseq-workshop/-/blob/master/Files/in_day1_rep1_chr12.bam?inline=false)
    * [in_day1_rep1_chr12.bam.bai](https://gitlab.com/ohri/2019-SCN-rnaseq-workshop/-/blob/master/Files/in_day1_rep1_chr12.bam.bai?inline=false)

Differential Gene Expression with DESeq2 (Chris Porter - June 2021) [PowerPoint](Files/2021_Workshop_Lecture_2_DESeq2.pptx?inline=false)
* [Salmon gene counts for Differential Gene Expression Analysis](http://www.ogic.ca/projects/2020_RNASeq_Workshop/RNA-seq_salmon_files.zip)
* [R Script for Differential Gene Expression Analysis](Files/DESeq2_analysis_script.Rmd?inline=false)
* [Ensembl annotation data](Files/genemap_data.txt?inline=false)

Differential Gene Expression with DESeq2 (Chris Porter - October 2021) [PowerPoint](Files/2021_Workshop_Lecture_2b_DESeq2.pptx?inline=false)
* [Salmon gene counts (MOV10) for DE analysis](Files/salmon_MOV10.zip?inline=false)
* [Transcript to gene mapping](Files/tx2gene_grch38_ens94.txt?inline=false)
* [Workshop analysis script (on-the-fly)](Files/workshop_analysis.Rmd) - currently as it was at the end of Thursday's presentation.

Gene Set Enrichment Analysis (Gareth Palidwor)  [PowerPoint](Files/2021_Workshop_Lecture_3_GeneAnnotationEnrichmentAnalysis.ppt?inline=false)
* [g:Profiler exercise instructions](Files/gprofiler.md)

RNA-seq Experimental Design  (Dr. Ted Perkins) [PDF](Files/Perkins_RNASeqWorkshop_2021.pdf)

### Day 2:

Recap DESeq2 analysis walk-through (session 1/June 10th 2021 only)
* [Ensembl annotation data](Files/genemap_data.txt?inline=false)
* [Updated DESeq2 R script](Files/DESeq2_analysis_recap.Rmd)

Introduction to the single-cell RNA sequencing workflow (David Cook) [PowerPoint](Files/scRNAseq_workshop_ppt.pptx)

[Single-cell data and notebooks](https://github.com/dpcook/scrna_seq_workshop_2021)
